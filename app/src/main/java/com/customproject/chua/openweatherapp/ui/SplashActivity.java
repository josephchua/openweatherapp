package com.customproject.chua.openweatherapp.ui;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.customproject.chua.openweatherapp.BuildConfig;
import com.customproject.chua.openweatherapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.app_version)
    TextView app_version;

    String version;
    int code;
    String buildVariant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
            code = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (BuildConfig.FLAVOR.equals("prod")) {
            buildVariant = "prod";
        } else if (BuildConfig.FLAVOR.equals("dev")) {
            buildVariant = "dev";
        } else {
            buildVariant = "test";
        }

        app_version.setText(buildVariant + " " + version + " " + code);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();

            }
        }, 2000);


    }

}
