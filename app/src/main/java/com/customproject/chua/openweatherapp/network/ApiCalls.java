package com.customproject.chua.openweatherapp.network;

import android.content.Context;
import android.util.Log;

import com.customproject.chua.openweatherapp.R;
import com.customproject.chua.openweatherapp.objects.CurrentWeather;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ApiCalls {

    public final static String appid = "52f97e3b097ff88fb3249f4ff65643b0";

    Context context;
    String url;
    CompositeDisposable compositeDisposable;
    ApiInterface apiInterface;

    public ApiCalls(Context context, CompositeDisposable compositeDisposable, ApiInterface apiInterface) {
        this.context = context;
        this.compositeDisposable = compositeDisposable;
        this.apiInterface = apiInterface;

        url = context.getResources().getString(R.string.base_url) +
                context.getResources().getString(R.string.weather)
                + "?q={q}&appid={appid}";


    }

    public void fetch(String q) {

        compositeDisposable.add(Rx2AndroidNetworking.get(url)
                .addPathParameter("q", q)
                .addPathParameter("appid", appid)
                .build()
                .getObjectSingle(CurrentWeather.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CurrentWeather>() {
                    @Override
                    public void accept(CurrentWeather currentWeather) throws Exception {

                        apiInterface.onFetchFinish(currentWeather);

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.e("throwable", throwable.getMessage());
                    }
                }));

    }

    public interface ApiInterface {

        void onFetchFinish(CurrentWeather currentWeather);

    }

}
