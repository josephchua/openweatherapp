package com.customproject.chua.openweatherapp.objects;

import java.util.List;

public class CurrentWeather {

    String name;
    List<Weather> weather;
    Temparature main;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public Temparature getMain() {
        return main;
    }

    public void setMain(Temparature main) {
        this.main = main;
    }
}
