package com.customproject.chua.openweatherapp.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.customproject.chua.openweatherapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ButtonFragment extends Fragment {

    @BindView(R.id.button)
    Button button;

    FragmentManager fragmentManager;
    Fragment listFragment;

    public ButtonFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_button, container, false);

        ButterKnife.bind(this, view);


        fragmentManager = getActivity().getSupportFragmentManager();
        listFragment = fragmentManager.findFragmentById(R.id.listFragment);

        return view;
    }

    @OnClick (R.id.button)
    public void onButtonClicked() {
        Log.e("Button", "clicked");
        listFragment.onAttach(getContext());
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


}
