package com.customproject.chua.openweatherapp.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.customproject.chua.openweatherapp.R;
import com.customproject.chua.openweatherapp.network.ApiCalls;
import com.customproject.chua.openweatherapp.objects.CurrentWeather;
import com.customproject.chua.openweatherapp.objects.CurrentWeatherHolder;
import com.mindorks.placeholderview.PlaceHolderView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

public class ListFragment extends Fragment implements ApiCalls.ApiInterface{

    @BindView(R.id.phv_weather)
    PlaceHolderView phv_weather;

    ApiCalls apiCalls;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list, container, false);

        ButterKnife.bind(this, view);

        phv_weather.getBuilder()
                .setHasFixedSize(false)
                .setItemViewCacheSize(10)
                .setLayoutManager(new LinearLayoutManager(getContext(),
                        LinearLayoutManager.VERTICAL, false));

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        setup();

    }

    public void setup() {

        try {
            phv_weather.removeAllViews();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        apiCalls = new ApiCalls(getContext(), compositeDisposable, this);

        apiCalls.fetch("London");
        apiCalls.fetch("Prague");
        apiCalls.fetch("San Francisco");

    }

    @Override
    public void onFetchFinish(CurrentWeather currentWeather) {
        phv_weather.addView(new CurrentWeatherHolder(currentWeather, getContext()));
        phv_weather.refresh();
    }
}

