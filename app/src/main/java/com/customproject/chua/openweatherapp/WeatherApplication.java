package com.customproject.chua.openweatherapp;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;

public class WeatherApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        AndroidNetworking.initialize(getApplicationContext());

    }
}
