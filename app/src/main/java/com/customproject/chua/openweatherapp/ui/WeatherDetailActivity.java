package com.customproject.chua.openweatherapp.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.androidnetworking.widget.ANImageView;
import com.customproject.chua.openweatherapp.R;
import com.customproject.chua.openweatherapp.network.ApiCalls;
import com.customproject.chua.openweatherapp.objects.CurrentWeather;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;

public class WeatherDetailActivity extends AppCompatActivity implements ApiCalls.ApiInterface {

    @BindView(R.id.current_name)
    TextView current_name;

    @BindView(R.id.weather_main)
    TextView weather_main;

    @BindView(R.id.weather_description)
    TextView weather_description;

    @BindView(R.id.weather_icon)
    ANImageView weather_icon;

    @BindView(R.id.temperature_temp)
    TextView temperature_temp;

    @BindView(R.id.temperature_pressure)
    TextView temperature_pressure;

    @BindView(R.id.temperature_humidity)
    TextView temperature_humidity;

    @BindView(R.id.temperature_min)
    TextView temperature_min;

    @BindView(R.id.temperature_max)
    TextView temperature_max;

    @BindView(R.id.button)
    Button button;

    ApiCalls apiCalls;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_detail);

        ButterKnife.bind(this);

        apiCalls = new ApiCalls(this, compositeDisposable, this);

        onRefreshClicked();

    }

    @OnClick(R.id.button)
    void onRefreshClicked() {
        apiCalls.fetch(getIntent().getExtras().getString("name"));
    }

    @Override
    public void onFetchFinish(CurrentWeather currentWeather) {
        current_name.setText(currentWeather.getName());
        weather_main.setText(currentWeather.getWeather().get(0).getMain());
        temperature_temp.setText(currentWeather.getMain().getTemp());
        weather_description.setText(currentWeather.getWeather().get(0).getDescription());
        weather_icon.setImageUrl(getResources().getString(R.string.img_url) + currentWeather.getWeather().get(0).getIcon() + getResources().getString(R.string.img_type));
        temperature_pressure.setText(currentWeather.getMain().getPressure());
        temperature_humidity.setText(currentWeather.getMain().getHumidity());
        temperature_min.setText(currentWeather.getMain().getTemp_min());
        temperature_max.setText(currentWeather.getMain().getTemp_max());
    }
}
