package com.customproject.chua.openweatherapp.objects;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.widget.TextView;

import com.androidnetworking.widget.ANImageView;
import com.customproject.chua.openweatherapp.R;
import com.customproject.chua.openweatherapp.ui.WeatherDetailActivity;
import com.mindorks.placeholderview.annotations.Animate;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.NonReusable;
import com.mindorks.placeholderview.annotations.Recycle;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;

@NonReusable
@Animate(Animate.CARD_TOP_IN_DESC)
@Layout(R.layout.currentweather_layout)
public class CurrentWeatherHolder {

    @View(R.id.card)
    CardView cardView;

    @View(R.id.current_name)
    TextView current_name;

    @View(R.id.weather_main)
    TextView weather_main;

    @View(R.id.weather_description)
    TextView weather_description;

    @View(R.id.weather_icon)
    ANImageView weather_icon;

    @View(R.id.temperature_temp)
    TextView temperature_temp;

    @View(R.id.temperature_pressure)
    TextView temperature_pressure;

    @View(R.id.temperature_humidity)
    TextView temperature_humidity;

    @View(R.id.temperature_min)
    TextView temperature_min;

    @View(R.id.temperature_max)
    TextView temperature_max;

    CurrentWeather currentWeather;
    Context context;

    public CurrentWeatherHolder(CurrentWeather currentWeather, Context context) {
        this.currentWeather = currentWeather;
        this.context = context;
    }

    @Resolve
    public void onResolved() {
        current_name.setText(currentWeather.getName());
        weather_main.setText(currentWeather.getWeather().get(0).getMain());
        temperature_temp.setText(currentWeather.getMain().getTemp());

        weather_description.setText(currentWeather.getWeather().get(0).getDescription());
        weather_icon.setImageUrl(context.getResources().getString(R.string.img_url) + currentWeather.getWeather().get(0).getIcon() + context.getResources().getString(R.string.img_type));
        temperature_pressure.setText(currentWeather.getMain().getPressure());
        temperature_humidity.setText(currentWeather.getMain().getHumidity());
        temperature_min.setText(currentWeather.getMain().getTemp_min());
        temperature_max.setText(currentWeather.getMain().getTemp_max());

        weather_description.setVisibility(android.view.View.GONE);
        weather_icon.setVisibility(android.view.View.GONE);
        temperature_pressure.setVisibility(android.view.View.GONE);
        temperature_humidity.setVisibility(android.view.View.GONE);
        temperature_min.setVisibility(android.view.View.GONE);
        temperature_max.setVisibility(android.view.View.GONE);

    }

    @Recycle
    public void onRecycled(){

    }

    @Click(R.id.card)
    public void onCardClick() {
        Log.e("onCardClick", currentWeather.getName());

        context.startActivity(new Intent(context, WeatherDetailActivity.class)
                .putExtra("name", currentWeather.getName()));

    }

}
